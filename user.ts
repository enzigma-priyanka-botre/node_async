//Input all details using module readline
const  readline = require('readline');

const rl = readline.createInterface({
        input: process.stdin,
        output: process.stdout
      });

      const question1 = () => {
        return new Promise<void>((resolve, reject) => {
          rl.question('Username : ', (answer) => {
            console.log(`Ur username is: ${answer}`)
            resolve()
          })
        })
      }
      
      const question2 = () => {
        return new Promise<void>((resolve, reject) => {
          rl.question('Password:  ', (answer) => {
            console.log(`Ur username is: ${answer}`)
            resolve()
          })
        })
      }

      const question3 = () => {
        return new Promise<void>((resolve, reject) => {
          rl.question('firstname : ', (answer) => {
            console.log(`Ur firstname is: ${answer}`)
            resolve()
          })
        })
      }

      const question4 = () => {
        return new Promise<void>((resolve, reject) => {
          rl.question('Lastname : ', (answer) => {
            console.log(`Ur Lastname is: ${answer}`)
            resolve()
          })
        })
      }

      const question5 = () => {
        return new Promise<void>((resolve, reject) => {
          rl.question('address : ', (answer) => {
            console.log(`Ur address is: ${answer}`)
            resolve()
          })
        })
      }
      
      const main = async () => {
        await question1()
        await question2()
        await question3()
        await question4()
        await question5()
        rl.close()
      }
      
      main();
     